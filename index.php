<!DOCTYPE html>
<html lang="pt-br"> 

<head>
<title>Taaz - Login</title> 
    <?php include('includes/template-part-head.php'); ?>
    <meta name="description" content="">
</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <div class="logo-forms">
                                        <svg width="153" height="155" viewBox="0 0 153 155" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M62.1719 1.27966C82.0284 -2.43898 102.575 2.02228 119.292 13.682C136.008 25.3416 147.526 43.2447 151.31 63.4526C154.132 78.5224 152.5 94.0761 146.62 108.147C140.74 122.218 130.876 134.173 118.276 142.502C105.675 150.831 90.904 155.159 75.8302 154.939C60.7565 154.719 46.0571 149.961 33.5909 141.266C21.1247 132.571 11.4515 120.33 5.79472 106.09C0.137899 91.8512 -1.24854 76.2534 1.81073 61.2695C4.87 46.2856 12.2376 32.5885 22.9818 21.9104C33.726 11.2323 47.3642 4.05278 62.1719 1.27966Z" fill="#EE7304"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M38.5628 85.0591C36.6981 83.3878 34.1962 79.7624 32.6088 77.6191C30.6685 74.9996 29.1193 72.8562 26.5726 70.1547C22.0469 65.3543 15.8935 58.2958 9.05301 62.7755C6.31061 64.5713 4.48315 68.2176 5.22435 71.9592C5.94035 75.5763 8.22542 76.7882 11.3088 79.7455C19.1684 87.2847 24.1214 93.5297 30.127 102.181C31.9805 104.851 33.4161 108.242 38.3179 108.865C41.7466 109.3 44.7386 107.717 46.747 104.774C57.6958 88.7302 65.4239 78.0695 81.6405 67.1425C90.2717 61.3267 101.622 56.2011 112.173 54.8925C113.942 54.6726 115.554 54.8033 117.281 54.5158C124.468 53.3191 127.476 43.5118 121.174 38.8598C116.258 35.2299 105.012 38.3376 100.884 39.4386C79.189 45.224 59.8313 59.4018 45.2601 76.848C44.0458 78.3018 39.6497 84.2791 38.5628 85.0591Z" fill="white"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M116.131 72.1175C106.992 71.7503 79.8252 83.8764 76.4821 91.7838C74.182 97.2234 77.6437 104.025 84.4022 104.131C90.4093 104.224 94.0125 96.2568 110.778 91.0715C115.287 89.6763 121.465 89.7668 123.772 84.242C126.024 78.8466 122.604 72.3775 116.131 72.1175Z" fill="white"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M112.254 107.283C107.141 107.218 101.986 111.947 100.999 116.014C99.8225 120.858 103.322 126.682 109.397 126.795C113.157 126.865 119.222 122.956 120.502 118.128C121.788 113.283 118.391 107.36 112.254 107.283Z" fill="white"/>
</svg>

                                        </div>
                                        
                                        <h1 class="h4 text-gray-900 mb-2">Seja bem vindo!</h1>
                                        <span class="info-forms">
                                        Bem vindos a Taaz, por gentileza insira suas credenciais para começar a utilizar o aplicativo.
                                        </span>
                                    </div>
                                    <form class="user">
                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-user"
                                                id="exampleInputEmail" aria-describedby="emailHelp"
                                                placeholder="Email...">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user"
                                                id="exampleInputPassword" placeholder="Senha">
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label" for="customCheck">Lembrar</label>
                                            </div>
                                        </div>
                                        <a href="index.html" class="btn btn-primary btn-user btn-block">
                                            Entrar
                                        </a>
                                        <hr>
                                        <a href="index.html" class="btn btn-google btn-user btn-block">
                                            <i class="fab fa-google fa-fw"></i> Logar com Google
                                        </a>
                                        <a href="index.html" class="btn btn-facebook btn-user btn-block">
                                            <i class="fab fa-facebook-f fa-fw"></i> Logar com Facebook
                                        </a>
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="forgot-password.html">Esqueceu sua senha?</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="register.html">Criar conta!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

<?php include('includes/template-part-footer.php')?>

</body>

</html>